/**
 * Created by sebby on 29/11/17.
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import { Projects } from '../api/projects/projects.js';
import Project from './components/project/Project.js';
import {Meteor} from 'meteor/meteor';
import AccountsUIWrapper from './components/userManagement/AccountsUIWrapper.js'


class App extends Component {

    constructor (props) {
        super(props);
        this.state = {
            hideCompleted: false,
        };
    }
    toggleHideCompleted() {
        this.setState({
           hideCompleted: !this.state.hideCompleted,
        });
    }


    handleSubmit(event) {
        event.preventDefault();

        //Getting the text by the React Reference
        const inputContent = ReactDOM.findDOMNode(this.refs.textInput).value.trim();
        console.log("You have written " + inputContent);
        //Insertion
        Meteor.call('projects.insert', inputContent);
        //Clear the input
        ReactDOM.findDOMNode(this.refs.textInput).value = '';
    }

    //Handle render
    renderProjects() {
        let filteredProjects = this.props.projects;
        if (this.state.hideCompleted == true) {
            filteredProjects = filteredProjects.filter(project => !project.checked);
        }

        return filteredProjects.map((project) => {
            const currentUserId = this.props.currentUser && this.props.currentUser._id;
            const showPrivateButton = project.owner === currentUserId;
            return (
                <Project
                    key={project._id}
                    project={project}
                    showPrivateButton={showPrivateButton}/>
            );

        });
    }
    render() {
        let toDisable="";
        if (!this.props.currentUser) {
            toDisable = "disabled field";
        }
        else {
            toDisable="field";
        }
        let formular = <form className="new-project ui form" onSubmit={this.handleSubmit.bind(this)} >
            <div className={toDisable}>
                <input name="first-name"  type="text" ref="textInput" placeholder="Type to add new project" />
            </div>
        </form>;

        return (
            <div className="container">

                    <div className="ui two column centered grid">
                        <div className="column">
                            <header>
                                <h1>Project List</h1>
                            </header>
                        </div>
                        <div className="four column centered row">
                            <div className="column">
                                {formular}
                                <label className="hide-completed">
                                    <input
                                        type="checkbox"
                                        readOnly
                                        checked={this.state.hideCompleted}
                                        onClick={this.toggleHideCompleted.bind(this)}
                                    />
                                    Hide Completed Projects ({this.props.incompleteCount})
                                </label>
                                <div className="ui vertical menu">
                                    {this.renderProjects()}
                                </div>
                                <AccountsUIWrapper />
                            </div>
                            <div className="column"></div>
                        </div>
                    </div>
            </div>
    );
    }
}
export default withTracker(() => {
    //"projects refer to the name of the publication
    handler = Meteor.subscribe('projects');
    console.log(handler.ready());
    return {
        projects: Projects.find({}, {sort: {createdAt:-1}}).fetch(),
        incompleteCount: Projects.find({checked: {$ne: true} }).count(),
        currentUser: Meteor.user(),
    };
})(App);