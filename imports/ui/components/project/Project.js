/**
 * Created by sebby on 29/11/17.
 */
import React from 'react';
import { Projects } from '../../../api/projects/projects.js';
import  {Meteor} from 'meteor/meteor';
import classnames from 'classnames';

export default class Project extends React.Component {
    toggleChecked() {
        // Set the checked property to the opposite of its current value
        Meteor.call('projects.setChecked', this.props.project._id, !this.props.project.checked);
    }
    togglePrivate() {
        Meteor.call('projects.setPrivate', this.props.project._id, ! this.props.project.private);
    }
    deleteThisProject() {
        Meteor.call('projects.remove', this.props.project._id);
    }
    render() {
        const projectClassName = classnames({
            checked: this.props.project.checked,
            private: this.props.project.private,
        });
        return(
            <div className={projectClassName}>
                <a className="active teal item">
                    {this.props.project.text}
                    <div className="ui teal left pointing label">1</div>
                    <button className="delete" onClick={this.deleteThisProject.bind(this)}>
                        &times;
                    </button>
                    <input
                        type="checkbox"
                        readOnly
                        checked={!!this.props.project.checked}
                        onClick={this.toggleChecked.bind(this)}
                    />
                    { this.props.showPrivateButton ? (

                        <button className="toggle-private ui icon button" onClick={this.togglePrivate.bind(this)}>
                            <i className={ this.props.project.private ? 'lock icon' : 'unlock alternate icon' }></i>
                        </button>
                    ) : ''}
                </a>

            </div>

        )
    }
}