/**
 * Created by sebby on 30/11/17.
 */

import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';



export const Projects = new Mongo.Collection('projects');

Projects.schema = new SimpleSchema({
    _id: { type: String, regEx: SimpleSchema.RegEx.Id },
    text: { type: String },
    owner: { type: String, regEx: SimpleSchema.RegEx.Id},
});

Projects.attachSchema(Projects.schema);
Meteor.methods({

    'projects.setPrivate'(projectId, setToPrivate) {
        check(projectId, String);
        check(setToPrivate, Boolean);
        const project = Projects.findOne(projectId);
        if (project.owner !== this.userId) {
            throw new Meteor.Error('not-authorized');
        }
        Projects.update(projectId, {$set:{private: setToPrivate}});
    },


    'projects.insert'(text) {
        check(text, String);
        if (!this.userId) {
            throw new Meteor.Error('not-authorized');
        }
        console.log(this.userId)
        Projects.insert({
            text,
            createdAt: new Date(),
            owner: this.userId,
            username: Meteor.users.findOne(this.userId).username,
        });

    },
    'projects.remove'(projectId) {
        check(projectId, String);

        Projects.remove(projectId);

    },
    'projects.setChecked'(projectId, setChecked) {
        check(projectId, String);
        check(setChecked, Boolean);

        Projects.update(projectId, {$set: { checked: setChecked } });
    }
});