/**
 * Created by sebby on 30/11/17.
 */
import { Accounts } from 'meteor/accounts-base';

Accounts.ui.config(
    {
        passwordSignupFields: 'USERNAME_ONLY',
    }
);
